from rest_framework import pagination

class CustomPagination(pagination.LimitOffsetPagination):
    page_size = 20
    max_limit = 50