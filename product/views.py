from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.authentication import SessionAuthentication
from rest_framework import permissions, views
from rest_framework.response import Response
# Create your views here.
from .models import Product
from .serializers import ProductSerializer
# from shareibc.pagination import CustomPagination
def filepath_images(domain, filename):
    return 'http://{domain}/media/background/{filename}'.format(domain=domain, filename=filename)



class ProductAPI(ListAPIView):
    authentication_classes = [SessionAuthentication]
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    serializer_class = ProductSerializer
    search_fields = ('name','type__name')
    ordering_fields = ('name', 'price','value','type__name','quantity')
    queryset = Product.objects.all()

    # def get_queryset(self):
    #     qs = Product.objects.all()
    #     query = self.request.GET.get('p')
    #     if query is not None:
    #         qs = qs.filter(name__icontains=query)
    #     return qs

class Images(views.APIView):
    authentication_classes = [SessionAuthentication]
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get(self,request, format=None):
        return Response(filepath_images(request.META['HTTP_HOST'],'sydney_5.jpg'))

class ProductDetailsAPI(RetrieveAPIView):
    authentication_classes = [SessionAuthentication]
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    lookup_field = 'id'


