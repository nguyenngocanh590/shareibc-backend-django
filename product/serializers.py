from rest_framework import serializers
from .models import Product, ProductImage

class ImageSeriallizer(serializers.ModelSerializer):
    class Meta:
        model = ProductImage
        fields = ('image', 'image_for')

class ProductSerializer(serializers.ModelSerializer):
    images_product = ImageSeriallizer(many=True)
    class Meta:
        model = Product
        fields = ('id','name', 'company', 'price', 'value', 'description', 'type', 'quantity', 'images_product')
