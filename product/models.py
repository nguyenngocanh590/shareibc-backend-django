from django.db import models
from datetime import datetime

# Create your models here.
def filepath_images(instance, filename):
    title = instance.product.name
    return 'products/%s-%s' % (title, filename)

def filepath_front_images(instance, filename):
    title = instance.name
    return 'products/%s-%s' % (title, filename)

class Type(models.Model):
    name = models.CharField(max_length=255, primary_key=True)
    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255)
    company = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    value = models.DecimalField(max_digits=6, decimal_places=2)
    date = models.DateTimeField(auto_now_add=True, blank=True)
    description = models.TextField(max_length=100000, default='')
    type = models.ForeignKey(Type, on_delete=models.SET(None))
    quantity = models.IntegerField(default=1)
    front_images = models.ImageField(upload_to=filepath_front_images, default=None)

    def __str__(self):
        return self.name

class ProductImage(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='images_product')
    image = models.ImageField(upload_to=filepath_images)
    image_for = models.CharField(max_length=264, default=None)

    def __str__(self):
        return self.product.name