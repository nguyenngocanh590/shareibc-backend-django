#!/usr/bin/env bash

set -e

# TODO: Set to URL of git repo.
PROJECT_GIT_URL='https://github.com/joelNguyenn1010/shareibc-backend-django.git'

PROJECT_BASE_PATH='/usr/local/apps'
VIRTUALENV_BASE_PATH='/usr/local/virtualenvs'

# Set Ubuntu Language
locale-gen en_GB.UTF-8

# Install Python, SQLite and pip
echo "Installing dependencies..."
apt-get update
apt-get install -y python3-dev python3-venv sqlite python-pip supervisor nginx git

mkdir -p $PROJECT_BASE_PATH
git clone $PROJECT_GIT_URL $PROJECT_BASE_PATH/shareibc

mkdir -p $VIRTUALENV_BASE_PATH
python3 -m venv $VIRTUALENV_BASE_PATH/shareibc

$VIRTUALENV_BASE_PATH/shareibc/bin/pip install -r $PROJECT_BASE_PATH/shareibc/requirements.txt

# Run migrations
cd $PROJECT_BASE_PATH/shareibc

# Setup Supervisor to run our uwsgi process.
cp $PROJECT_BASE_PATH/shareibc/deploy/supervisor_shareibc.conf /etc/supervisor/conf.d/shareibc.conf
supervisorctl reread
supervisorctl update
supervisorctl restart profiles_api

# Setup nginx to make our application accessible.
cp $PROJECT_BASE_PATH/shareibc/deploy/shareibc-nginx.conf /etc/nginx/sites-available/shareibc.conf
rm /etc/nginx/sites-enabled/default
ln -s /etc/nginx/sites-available/shareibc.conf /etc/nginx/sites-enabled/shareibc.conf
systemctl restart nginx.service

echo "DONE! :)"