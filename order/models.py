from django.db import models
from product.models import Product
# Create your models here.

from django.contrib.auth.models import User


class Status(models.Model):
    status = models.CharField(max_length=225, default='processing')

class OrderDetail(models.Model):
    address = models.TextField(default='', blank=False, null=False)
    city = models.CharField(max_length=255, default='', blank=False, null=False)
    postcode = models.IntegerField(blank=False, null=False)
    date = models.DateTimeField(auto_now_add=True, blank=True)
    first_name = models.CharField(max_length=255, blank=False, null=False)
    last_name = models.CharField(max_length=255, blank=False, null=False)
    email = models.EmailField(blank=False)
    total_price = models.DecimalField(max_digits=12, decimal_places=2, blank=False, null=False)
    payment_ref_id = models.TextField(blank=False, null=False, default='cant get id')
    status = models.ForeignKey(Status,default=1, related_name='order_status', on_delete=models.CASCADE)

    @property
    def orders(self):
        return Order.objects.all()

class Order(models.Model):
    details = models.ForeignKey(OrderDetail,on_delete=models.CASCADE)
    products = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(blank=False, null=False)


class Order_Product(models.Model):
    orders = models.ForeignKey(Order, on_delete=models.CASCADE)
    products = models.ForeignKey(Product, on_delete=models.CASCADE)
