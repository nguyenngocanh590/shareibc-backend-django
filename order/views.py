from django.shortcuts import render
from rest_framework.response import Response
from decimal import Decimal

# Create your views here.
from django.shortcuts import render
from rest_framework.generics import ListAPIView, CreateAPIView

from rest_framework.authentication import SessionAuthentication
import stripe

from shareibc.settings import STRIPE_SECRET_KEY, STRIPE_PUBLIC_KEY
stripe.api_key = STRIPE_SECRET_KEY

from rest_framework import permissions, views, mixins, status
# Create your views here.
from product.models import Product
from .models import Order, OrderDetail, Status
from .serializers import OrderSerializer, OrderDetailsSerializer,DetailsSe


def countTotalPrice(orders):
    total_price = Decimal(0.0)
    for o in orders:
        k = Product.objects.filter(id=o.get('products')).values('price')
        total_price+=(k.first().get('price')*o.get('quantity'))
        print(total_price)
    return int(total_price*100)

class OrderCreateAPI(CreateAPIView):
    permission_classes = []
    authentication_classes = []
    serializer_class = OrderDetailsSerializer

    def post(self, request,*args, **kwargs):

        orders = request.data['orders']
        total_price = countTotalPrice(orders)
        token = request.data.pop('token')
        print(request.data)
        details = OrderDetailsSerializer(data=request.data)
        print(details.is_valid())
        if details.is_valid():
            if len(orders) > 0:
                try:
                    charge = stripe.Charge.create(
                           amount=total_price,
                           currency="usd",
                           source= token,
                           description="The product charged to the user"
                      )
                    self.create(request, *args, **kwargs)
                    return Response("Success charge")
                except Exception as e:
                    print("herere")
                    print(e)
                    return Response({'Erorr':"Errorr"})
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)

class OrderAPI(ListAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = OrderSerializer

    def get_queryset(self):
        qs = Order.objects.all()
        return qs



class Test(CreateAPIView):
    permission_classes = []
    authentication_classes = []
    serializer_class = OrderDetailsSerializer
