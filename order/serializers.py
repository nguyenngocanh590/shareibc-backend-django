from rest_framework import serializers
from .models import Order, OrderDetail
from rest_framework.response import Response

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ['details','products', 'quantity']
        read_only_fields = ('details',)

class DetailsSe(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'

class OrderDetailsSerializer(serializers.ModelSerializer):
    orders = OrderSerializer(many=True)
    class Meta:
        model = OrderDetail
        fields = ['address','city','postcode','first_name','last_name','email','total_price', 'orders','status']
    #
    def create(self, validated_data):
        orders = validated_data.pop('orders')
        if len(orders) == 0:
            Response("fwefw")
            raise serializers.ValidationError("Errorroror")
        else:
            try:
                orderDetails = OrderDetail.objects.create(**validated_data)
            except:
                raise serializers.ValidationError("Errorroror")
            for order in orders:
                Order.objects.create(**order, details=orderDetails)
            return orderDetails

    # def create_card(self):
    #     # Token = request.data['token']
    #     # total_price = request.data['total_price']
    #     # orders = request.data['orders']
    #     details = request.data['details']
    #     d = DetailsSe(OrderDetail, data=details)
    #     print(details)
    #     print(d.is_valid())
    #     if d.is_valid():
    #         u = OrderDetail(d)
    #         u.save()
    #         d.save()
    #         # try:
    #         #     charge = stripe.Charge.create(
    #         #             amount=Decimal(total_price)*100,
    #         #             currency="usd",
    #         #             source= Token,
    #         #             description="The product charged to the user"
    #         #         )
    #         #
    #         #         # for order in orders:
    #         #         #     o = OrderSerializer(data=order)
    #         #         #     if o.is_valid():
    #         #         #       o.save()
    #         #     return Response("Success")
    #         # except Exception as e:
    #         #     return Response("No card")
    #         return Response(d.is_valid())
    #     return Response("Error")

